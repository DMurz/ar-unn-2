﻿using App.Code.Core;
using App.Code.Dependencies;
using UnityEngine;

namespace App.Code.Tools
{
    public class EditorRaycastService : IRaycastService
    {
        private class RaycastInfo : IRaycastHit
        {
            public PlaneType Type { get; }
            public Vector3 Point { get; }
            public Quaternion Rotation { get; }
            public GameObject Target { get; }

            public RaycastInfo(RaycastHit hit)
            {
                Point = hit.point;
                Rotation = Quaternion.identity;
                Target = hit.collider.gameObject;

                var editorPlane = Target.GetComponent<EditorPlane>();
                Type = editorPlane
                    ? editorPlane.Type
                    : PlaneType.Null;
            }
        }

        private static IPlayerProvider PlayerProvider =>
            Locator.Get<IPlayerProvider>();

        public IRaycastHit Raycast(Vector2 screenPos)
        {
            var ray = PlayerProvider.Player.ScreenPointToRay(screenPos);
            if (Physics.Raycast(ray, out var hit))
            {
                if (hit.collider.gameObject.GetComponent<EditorPlane>() != null)
                    return new RaycastInfo(hit);

                return null;
            }

            return null;
        }
    }
}