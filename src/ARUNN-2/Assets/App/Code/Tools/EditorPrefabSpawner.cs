﻿using UnityEngine;

namespace App.Code.Tools
{
    public class EditorPrefabSpawner : MonoBehaviour
    {
#if UNITY_EDITOR
        public GameObject[] Objects;

        private void Awake()
        {
            foreach (var obj in Objects)
                Instantiate(obj);
        }
#endif
    }
}