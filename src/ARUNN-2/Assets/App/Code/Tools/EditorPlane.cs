﻿using App.Code.Core;
using UnityEngine;

namespace App.Code.Tools
{
    public class EditorPlane : MonoBehaviour
    {
        public PlaneType Type;

#if !UNITY_EDITOR
        private void Awake()
        {
            gameObject.SetActive(false);
        }
#endif
    }
}