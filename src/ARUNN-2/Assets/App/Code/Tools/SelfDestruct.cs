﻿using UnityEngine;

namespace App.Code.Tools
{
    public class SelfDestruct : MonoBehaviour
    {
        public float TimeToDestroy = 1.0f;

        private float _timer;

        private void Update()
        {
            _timer += Time.deltaTime;
            if (_timer >= TimeToDestroy)
                Destroy(gameObject);
        }
    }
}