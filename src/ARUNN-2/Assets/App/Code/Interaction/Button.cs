﻿using System;
using UnityEngine;

namespace App.Code.Interaction
{
    public class Button : MonoBehaviour,
        IButton
    {
        public event Action OnBegin;
        public event Action OnEnd;
        public event Action OnClick;

        public void Begin() =>
            OnBegin?.Invoke();

        public void End() =>
            OnEnd?.Invoke();

        public void Click() =>
            OnClick?.Invoke();
    }
}