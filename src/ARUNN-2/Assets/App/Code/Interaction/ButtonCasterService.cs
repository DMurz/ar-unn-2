﻿using System;
using App.Code.Core;
using App.Code.Dependencies;
using App.Code.Input;
using UnityEngine;

namespace App.Code.Interaction
{
    public class ButtonCasterService : MonoBehaviour, 
        IValidTouchService
    {
        public event Action<ITouch> OnTouch;

        private ITouch _currentTouch;
        private IButton _currentButton;

        private static IScreenService ScreenService =>
            Locator.Get<IScreenService>();

        private static IPlayerProvider PlayerProvider =>
            Locator.Get<IPlayerProvider>();

        private void Awake() =>
            ScreenService.OnTouch += ReceiveTouch;

        private void OnDestroy() =>
            ScreenService.OnTouch -= ReceiveTouch;

        private void ReceiveTouch(ITouch touch)
        {
            if (_currentTouch != null && _currentTouch.Id != touch.Id)
            {
                OnTouch?.Invoke(touch);
                return;
            }

            if (touch.Phase == TouchPhase.Began)
            {
                _currentButton = Cast(touch.Position);
                if (_currentButton != null)
                    _currentButton.Begin();
                else
                    OnTouch?.Invoke(touch);
            }
            else if (touch.Phase == TouchPhase.Ended)
            {
                var button = Cast(touch.Position);
                if (button != null && button == _currentButton)
                {
                    _currentButton.End();
                    _currentButton.Click();
                }
                else if (_currentButton != null)
                    _currentButton.End();
                else
                    OnTouch?.Invoke(touch);

                _currentButton = null;
            }
        }

        private IButton Cast(Vector2 pos)
        {
            var ray = PlayerProvider.Player.ScreenPointToRay(pos);
            if (!Physics.Raycast(ray, out var hit)) return null;

            return hit.collider.gameObject.GetComponentInChildren<IButton>();
        }
    }
}