using System;
using App.Code.Input;

namespace App.Code.Interaction
{
    public interface IValidTouchService
    {
        public event Action<ITouch> OnTouch;
    }
}