namespace App.Code.Interaction
{
    public interface IButton
    {
        public void Begin();
        public void End();
        public void Click();
    }
}