﻿using App.Code.AR;
using App.Code.Configurator;
using App.Code.Configurator.Model;
using App.Code.Core;
using App.Code.Game;
using App.Code.Input;
using App.Code.Interaction;
using App.Code.Tools;
using App.Code.UI;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace App.Code.Dependencies
{
    public class AppInstaller : MonoBehaviour
    {
        public ARRaycastManager RaycastManager;
        public PlayerProvider PlayerProvider;
        public ScreenService ScreenService;
        public ARTrackedImageManager TrackedImageManager;
        public MultipleImageTracking MultipleImageTracking;
        public Mediator Mediator;
        public MoveService MoveService;
        public ButtonCasterService ButtonCasterService;
        public CoinService CoinService;
        public ScoreService ScoreService;
        public ConfigurationService ConfigurationService;
        public SODatabase Database;
        public ConfiguratorModelService ConfiguratorModelService;

        private void Awake()
        {
            Locator.Set(RaycastManager);
            Locator.Set<IPlayerProvider>(PlayerProvider);
            Locator.Set<IScreenService>(ScreenService);
            Locator.Set(TrackedImageManager);
            Locator.Set<IImageService>(new ImageService());
            Locator.Set(MultipleImageTracking);
            Locator.Set<IMediator>(Mediator);
            Locator.Set<IMoveService>(MoveService);
            Locator.Set<IEntityService>(new EntityService());
            Locator.Set<IValidTouchService>(ButtonCasterService);
            Locator.Set<ICoinService>(CoinService);
            Locator.Set<IScoreService>(ScoreService);
            Locator.Set<IConfigurationService>(ConfigurationService);
            Locator.Set<ISODatabase>(Database);
            Locator.Set(ConfiguratorModelService);

#if UNITY_EDITOR
            Locator.Set<IRaycastService>(new EditorRaycastService());
#else
            Locator.Set<IRaycastService>(new ARRaycastService(RaycastManager));
#endif
        }
    }
}