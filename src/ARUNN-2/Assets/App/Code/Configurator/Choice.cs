using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator
{
    public class Choice : ScriptableObject
    {
        [ReadOnly] public string Id;

        [SerializeField, HideInInspector] private Step _parent;

#if UNITY_EDITOR
        public void Init(Step parent)
        {
            _parent = parent;
            GenerateId();
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void Rename(string newName)
        {
            name = newName;
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void Delete() =>
            _parent.Delete(this);

        private void GenerateId() =>
            Id = Guid.NewGuid().ToString();
#endif
    }
}