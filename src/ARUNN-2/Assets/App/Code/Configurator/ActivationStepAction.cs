namespace App.Code.Configurator
{
    public class ActivationStepAction : StepAction
    {
        public override void Do() => 
            gameObject.SetActive(true);

        public override void UnDo() => 
            gameObject.SetActive(false);
    }
}