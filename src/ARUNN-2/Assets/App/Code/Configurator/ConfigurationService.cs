﻿using System;
using System.Collections.Generic;
using App.Code.Configurator.Model;
using App.Code.Configurator.UI;
using App.Code.Dependencies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator
{
    public class ConfigurationService : MonoBehaviour,
        IConfigurationService
    {
        public ConfigurationView MainView;
        [InlineEditor] public Configuration Config;
        public StepView[] Views;

        public event Action<Step, Choice> OnStepValueChanged;

        private static ConfiguratorModelService ConfiguratorModelService =>
            Locator.Get<ConfiguratorModelService>();

        private List<ConfigureObject> _configureObjects = new List<ConfigureObject>(5);

        private void Awake() =>
            ConfiguratorModelService.OnModelLoaded += ModelLoaded;

        private void OnDestroy() =>
            ConfiguratorModelService.OnModelLoaded -= ModelLoaded;

        public void ObjectSpawned(ConfigureObject configureObject)
        {
            _configureObjects.Add(configureObject);
            configureObject.ApplyModel(ConfiguratorModelService.Model);
            if (!MainView.IsShowed)
                MainView.Show();
        }

        public void ObjectDestroyed(ConfigureObject configureObject)
        {
            _configureObjects.Remove(configureObject);
            if (_configureObjects.Count <= 0 && MainView.IsShowed)
                MainView.Hide();
        }


        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        public void SetValue(Step step, Choice value) =>
            OnStepValueChanged?.Invoke(step, value);

        private void ModelLoaded(ConfiguratorModel model)
        {
            foreach (var configureObject in _configureObjects)
                configureObject.ApplyModel(model);
        }
    }
}