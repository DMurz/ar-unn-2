using System;
using App.Code.Configurator.Model;

namespace App.Code.Configurator
{
    public interface IConfigurationService
    {
        public event Action<Step, Choice> OnStepValueChanged;
        public void SetValue(Step step, Choice value);
        public void ObjectSpawned(ConfigureObject configureObject);
        public void ObjectDestroyed(ConfigureObject configureObject);
    }
}