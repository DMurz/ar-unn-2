﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace App.Code.Configurator
{
    public class SODatabase : MonoBehaviour,
        ISODatabase
    {
        public Step[] Steps;
        public Choice[] Choices;

        private Dictionary<string, Step> _steps;
        private Dictionary<string, Choice> _choices;

        private void Awake()
        {
            _steps = Steps.ToDictionary(x => x.Id);
            _choices = Choices.ToDictionary(x => x.Id);
        }

        public Step GetStep(string id) =>
            _steps[id];

        public Choice GetChoice(string id) =>
            _choices[id];
    }
}