﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator
{
    [CreateAssetMenu(fileName = "Configuration", menuName = "Create Configuration")]
    public class Configuration : ScriptableObject
    {
        [InlineEditor] public Step[] Steps;
    }
}