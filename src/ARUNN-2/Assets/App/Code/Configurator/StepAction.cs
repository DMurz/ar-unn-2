﻿using UnityEngine;

namespace App.Code.Configurator
{
    public abstract class StepAction : MonoBehaviour
    {
        public Choice LinkTo;
        public StepView StepView { get; private set; }

        public void Init(StepView stepView)
        {
            StepView = stepView;
            PostInit();
        }

        protected virtual void PostInit() { }
        public abstract void Do();
        public abstract void UnDo();
    }
}