﻿using App.Code.Configurator.Model;
using App.Code.Dependencies;
using UnityEngine;

namespace App.Code.Configurator
{
    public class ConfigureObject : MonoBehaviour
    {
        public Material MaterialInstance { get; private set; }

        public Renderer[] TargetRenderer;
        public StepView[] StepViews;

        [SerializeField] private Material _targetMaterial;

        private static IConfigurationService ConfigurationService =>
            Locator.Get<IConfigurationService>();

        private void Awake()
        {
            MaterialInstance = Instantiate(_targetMaterial);
            foreach (var r in TargetRenderer)
                r.material = MaterialInstance;

            foreach (var stepView in StepViews)
                stepView.Init(this);

            ConfigurationService.ObjectSpawned(this);
        }

        private void OnDestroy() =>
            ConfigurationService.ObjectDestroyed(this);

        public void ApplyModel(ConfiguratorModel model)
        {
            foreach (var stepView in StepViews) 
                stepView.ApplyModel(model);
        }
    }
}