using UnityEngine;

namespace App.Code.Configurator
{
    public class ScaleStepAction : StepAction
    {
        public Transform Target;
        public Vector3 TargetScale;

        private Vector3 _default;

        protected override void PostInit() => 
            _default = Target.localScale;

        public override void Do() =>
            Target.localScale = TargetScale;

        public override void UnDo() =>
            Target.localScale = _default;
    }
}