﻿using App.Code.Dependencies;
using UnityEngine;

namespace App.Code.Configurator.UI
{
    public class ConfigurationView : MonoBehaviour
    {
        public GameObject PrevButton;
        public GameObject NextButton;

        public StepView[] Steps;
        
        public bool IsShowed { get; private set; }

        private static IConfigurationService ConfigurationService =>
            Locator.Get<IConfigurationService>();

        private int _currentStep = -1;

        private void Awake()
        {
            foreach (var stepView in Steps)
            {
                stepView.Init(this);
                stepView.Hide();
            }

            SetStep(0);
            gameObject.SetActive(false);
        }

        public void Show()
        {
            gameObject.SetActive(true);
            IsShowed = true;
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            IsShowed = false;
        }

        public void SelectChoice(Step step, Choice value) =>
            ConfigurationService.SetValue(step, value);

        public void Next()
        {
            if (_currentStep == Steps.Length - 1) return;

            SetStep(_currentStep + 1);
        }

        public void Prev()
        {
            if (_currentStep == 0) return;

            SetStep(_currentStep - 1);
        }

        private void SetStep(int step)
        {
            if (step == _currentStep) return;

            PrevButton.SetActive(true);
            NextButton.SetActive(true);

            if (step == 0)
                PrevButton.SetActive(false);
            else if (step == Steps.Length - 1)
                NextButton.SetActive(false);

            if (_currentStep != -1)
                HideStep(_currentStep);

            _currentStep = step;
            ShowStep(_currentStep);
        }

        private void HideStep(int step)
        {
            if (step >= 0 && step < Steps.Length)
                Steps[step].Hide();
        }

        private void ShowStep(int step)
        {
            if (step >= 0 && step < Steps.Length)
                Steps[step].Show();
        }
    }
}