﻿using UnityEngine;
using UnityEngine.UI;

namespace App.Code.Configurator.UI
{
    public class StepChoice : MonoBehaviour
    {
        public Choice Choice;

        public Image TargetImage;
        public Color DeselectedColor;
        public Color SelectedColor;

        private StepView _stepView;

        public void Init(StepView stepView) =>
            _stepView = stepView;

        public void Click() =>
            _stepView.Select(Choice);

        public void Select() =>
            TargetImage.color = SelectedColor;

        public void Deselect() =>
            TargetImage.color = DeselectedColor;
    }
}