﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator.UI
{
    public class StepView : MonoBehaviour
    {
        [InlineEditor] public Step LinkedTo;
        public StepChoice[] StepChoices;

        private ConfigurationView _configurationView;
        private Choice _prevStep;
        private Dictionary<Choice, StepChoice> _choices;

        public void Init(ConfigurationView view)
        {
            _choices = StepChoices.ToDictionary(x => x.Choice);
            _configurationView = view;
            foreach (var stepChoice in StepChoices) 
                stepChoice.Init(this);
        }

        public void Select(Choice value)
        {
            _configurationView.SelectChoice(LinkedTo, value);
            if (_prevStep != null)
            {
                if (_prevStep == value)
                {
                    if (LinkedTo.IsCancellable)
                    {
                        _choices[_prevStep].Deselect();
                        _prevStep = null;
                        return;
                    }

                    return;
                }

                _choices[_prevStep].Deselect();
                _choices[value].Select();
            }
            else
                _choices[value].Select();

            _prevStep = value;
        }

        public void Show() => 
            gameObject.SetActive(true);

        public void Hide() => 
            gameObject.SetActive(false);
    }
}