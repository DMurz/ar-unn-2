using UnityEngine;

namespace App.Code.Configurator
{
    public class ColorStepAction : StepAction
    {
        public Color Default;
        public Color Target;

        public override void Do() =>
            StepView.ConfigureObject.MaterialInstance.color = Target;

        public override void UnDo() =>
            StepView.ConfigureObject.MaterialInstance.color = Default;
    }
}