﻿using System;
using App.Code.Dependencies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator.Model
{
    public class ConfiguratorModelService : MonoBehaviour
    {
        public event Action<ConfiguratorModel> OnModelLoaded;

        public ConfiguratorModel Model;

        private static IConfigurationService ConfigurationService =>
            Locator.Get<IConfigurationService>();

        private void Awake() =>
            ConfigurationService.OnStepValueChanged += Save;

        private void OnDestroy() =>
            ConfigurationService.OnStepValueChanged -= Save;

        private void Save(Step step, Choice choice) =>
            Model.Save(step, choice);

        [Button(ButtonStyle.FoldoutButton)]
        private string ToJson() =>
            JsonUtility.ToJson(Model);

        [Button(ButtonStyle.FoldoutButton)]
        private void Load(string json)
        {
            Model = JsonUtility.FromJson<ConfiguratorModel>(json);
            OnModelLoaded?.Invoke(Model);
        }
    }
}