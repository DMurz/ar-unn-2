using System;
using App.Code.Dependencies;

namespace App.Code.Configurator.Model
{
    [Serializable]
    public class ChoiceModel
    {
        public Step Step =>
            Locator.Get<ISODatabase>().GetStep(StepId);

        public Choice Choice =>
            Locator.Get<ISODatabase>().GetChoice(ChoiceId);

        public string StepId;
        public string ChoiceId;
    }
}