﻿using System;
using System.Collections.Generic;

namespace App.Code.Configurator.Model
{
    [Serializable]
    public class ConfiguratorModel
    {
        public List<ChoiceModel> Choices;

        public void Save(Step step, Choice choice)
        {
            Choices ??= new List<ChoiceModel>();

            var choiceModel = Choices.Find(x => x.StepId == step.Id);
            if (choiceModel != null)
                choiceModel.ChoiceId = choiceModel.ChoiceId == choice.Id && step.IsCancellable
                    ? ""
                    : choice.Id;
            else
                Choices.Add(new ChoiceModel
                {
                    ChoiceId = choice.Id,
                    StepId = step.Id
                });
        }
    }
}