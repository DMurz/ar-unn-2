﻿namespace App.Code.Configurator
{
    public interface ISODatabase
    {
        public Step GetStep(string id);
        public Choice GetChoice(string id);
    }
}