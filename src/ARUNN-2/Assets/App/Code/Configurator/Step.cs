﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator
{
    [CreateAssetMenu(fileName = "Step", menuName = "Create Step")]
    public class Step : ScriptableObject
    {
        [ReadOnly] public string Id;
        public string Name;
        public bool IsCancellable;
        [SerializeField, HideInInspector] private List<Choice> _choices;

#if UNITY_EDITOR
        [Button(ButtonStyle.FoldoutButton)]
        private void CreateChoice(string choiceName)
        {
            _choices ??= new List<Choice>();
            var instance = CreateInstance<Choice>();
            instance.name = choiceName;
            instance.Init(this);
            _choices.Add(instance);
            UnityEditor.AssetDatabase.AddObjectToAsset(instance, this);
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

        public void Delete(Choice choice)
        {
            _choices.Remove(choice);
            UnityEditor.AssetDatabase.RemoveObjectFromAsset(choice);
            DestroyImmediate(choice, true);

            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void GenerateId()
        {
            if (string.IsNullOrEmpty(Id))
                Id = Guid.NewGuid().ToString();
        }
#endif
    }
}