﻿using System.Collections.Generic;
using System.Linq;
using App.Code.Configurator.Model;
using App.Code.Dependencies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Configurator
{
    public class StepView : MonoBehaviour
    {
        public ConfigureObject ConfigureObject { get; private set; }

        [InlineEditor] public Step LinkedTo;
        public StepAction[] Objects;

        private static IConfigurationService ConfigurationService =>
            Locator.Get<IConfigurationService>();

        private Choice _prevObject;
        private Dictionary<Choice, StepAction> _stepActions;

        public void Init(ConfigureObject configureObject)
        {
            _stepActions = Objects.ToDictionary(x => x.LinkTo);
            ConfigureObject = configureObject;
            foreach (var stepAction in Objects) 
                stepAction.Init(this);

            ConfigurationService.OnStepValueChanged += StepValueChanged;
            foreach (var obj in Objects)
                obj.UnDo();
        }

        private void OnDestroy() =>
            ConfigurationService.OnStepValueChanged -= StepValueChanged;

        public void SetValue(Choice value)
        {
            if (value == _prevObject && LinkedTo.IsCancellable)
            {
                _stepActions[_prevObject].UnDo();
                _prevObject = null;
                return;
            }

            if (value == _prevObject)
                return;

            if (_prevObject != null)
                _stepActions[_prevObject].UnDo();

            _stepActions[value].Do();
            _prevObject = value;
        }

        public void ApplyModel(ConfiguratorModel model)
        {
            foreach (var choice in model.Choices)
            {
                if (choice.StepId == LinkedTo.Id)
                {
                    SetValue(choice.Choice);
                    return;
                }
            }
        }

        private void StepValueChanged(Step step, Choice value)
        {
            if (step == LinkedTo)
                SetValue(value);
        }
    }
}