using UnityEngine;

namespace App.Code
{
    public class Move : MonoBehaviour
    {
        public float Speed;

        private void Update()
        {
            var pos = transform.position;
            pos.y += Speed * Time.deltaTime;
            transform.position = pos;
        }
    }
}