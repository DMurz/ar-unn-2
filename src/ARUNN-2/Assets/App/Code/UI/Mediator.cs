﻿using App.Code.Dependencies;
using App.Code.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.UI
{
    public class Mediator : MonoBehaviour,
        IMediator
    {
        public GameObject Buttons;

        private static IMoveService MoveService =>
            Locator.Get<IMoveService>();

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        public void ShowButtons() =>
            Buttons.SetActive(true);

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        public void HideButtons() =>
            Buttons.SetActive(false);

        public void MoveUpButtonClick() =>
            MoveService.MoveUp();

        public void MoveLeftButtonClick() =>
            MoveService.MoveLeft();

        public void MoveRightButtonClick() =>
            MoveService.MoveRight();

        public void MoveDownButtonClick() =>
            MoveService.MoveDown();
    }
}