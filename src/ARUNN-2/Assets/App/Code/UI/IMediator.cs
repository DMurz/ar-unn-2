namespace App.Code.UI
{
    public interface IMediator
    {
        public void ShowButtons();
        public void HideButtons();
    }
}