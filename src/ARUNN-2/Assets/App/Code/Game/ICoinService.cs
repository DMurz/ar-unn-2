namespace App.Code.Game
{
    public interface ICoinService
    {
        public void EntitySpawned(GameEntity gameEntity);
        public void CollectCoin(Coin coin);
    }
}