﻿using System;
using App.Code.Dependencies;
using App.Code.Interaction;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace App.Code.Game
{
    public class GameEntity : MonoBehaviour
    {
        public float TimeToHold = 0.5f;
        public Renderer Renderer;
        public Button ObjectButton;

        private bool _isHovered;
        private float _holdTimer;

        private static IEntityService EntityService =>
            Locator.Get<IEntityService>();

        private static IMoveService MoveService =>
            Locator.Get<IMoveService>();

        private static ICoinService CoinService =>
            Locator.Get<ICoinService>();

        private void Awake()
        {
            EntityService.EntitySpawned();
            ObjectButton.OnBegin += MouseDown;
            ObjectButton.OnEnd += MouseExit;
            ObjectButton.OnClick += MouseClick;

            MoveService.OnMoveDown += MoveDown;
            MoveService.OnMoveUp += MoveUp;
            MoveService.OnMoveLeft += MoveLeft;
            MoveService.OnMoveRight += MoveRight;

            CoinService.EntitySpawned(this);
        }

        private void Update()
        {
            if (_isHovered)
                _holdTimer += Time.deltaTime;
        }

        private void OnCollisionEnter(Collision other)
        {
            var coin = other.gameObject.GetComponent<Coin>();
            if (coin) 
                CoinService.CollectCoin(coin);
        }

        private void OnDestroy()
        {
            EntityService.EntityDestroyed();
            ObjectButton.OnBegin -= MouseDown;
            ObjectButton.OnEnd -= MouseExit;
            ObjectButton.OnClick -= MouseClick;

            MoveService.OnMoveDown -= MoveDown;
            MoveService.OnMoveUp -= MoveUp;
            MoveService.OnMoveLeft -= MoveLeft;
            MoveService.OnMoveRight -= MoveRight;
        }

        private void MouseDown()
        {
            _isHovered = true;
            _holdTimer = 0.0f;
        }

        private void MouseExit() =>
            _isHovered = false;

        private void MouseClick()
        {
            if (_holdTimer >= TimeToHold)
                Destroy(gameObject);
            else
                Click();
        }

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        private void Click()
        {
            var material = Renderer.material;
            material.color = new Color(Random.value, Random.value, Random.value, 1.0f);
        }

        private void MoveUp() =>
            transform.localPosition += transform.forward * MoveService.Speed;

        private void MoveLeft() =>
            transform.localPosition -= transform.right * MoveService.Speed;

        private void MoveRight() =>
            transform.localPosition += transform.right * MoveService.Speed;

        private void MoveDown() =>
            transform.localPosition -= transform.forward * MoveService.Speed;
    }
}