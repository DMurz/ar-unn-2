using System;

namespace App.Code.Game
{
    public interface IMoveService
    {
        public float Speed { get; }
        public event Action OnMoveUp;
        public event Action OnMoveLeft;
        public event Action OnMoveRight;
        public event Action OnMoveDown;
        public void MoveUp();
        public void MoveLeft();
        public void MoveRight();
        public void MoveDown();
        public void IncreaseSpeed();
    }
}