﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Game
{
    public class ScoreService : MonoBehaviour,
        IScoreService
    {
        [ShowInInspector]
        public int Score
        {
            get => _score;
            set
            {
                if (value != _score)
                {
                    _score = value;
                    OnScoreChanged?.Invoke(_score);
                }
            }
        }

        public event Action<int> OnScoreChanged;

        private int _score;

        public void AddScore(int score) =>
            Score += score;
    }
}