﻿using App.Code.Dependencies;
using UnityEngine;

namespace App.Code.Game
{
    public class CoinService : MonoBehaviour,
        ICoinService
    {
        public Coin CoinPrefab;
        public int CoinsCount = 10;
        public float MaxRadius = 1.0f;

        private static IScoreService ScoreService =>
            Locator.Get<IScoreService>();

        public void EntitySpawned(GameEntity gameEntity)
        {
            var point = gameEntity.transform.position;
            for (var i = 0; i < CoinsCount; i++)
            {
                var coinPoint = Point(point, MaxRadius);
                var coin = Instantiate(CoinPrefab, coinPoint, Quaternion.identity);
            }
        }

        public void CollectCoin(Coin coin)
        {
            ScoreService.AddScore(coin.Score);
            Destroy(coin.gameObject);
        }

        private Vector3 Point(Vector3 origin, float radius)
        {
            var point = Random.insideUnitCircle * radius;
            return new Vector3(origin.x + point.x, origin.y, origin.z + point.y);
        }
    }
}