using System;

namespace App.Code.Game
{
    public interface IScoreService
    {
        public event Action<int> OnScoreChanged;
        public int Score { get; }
        public void AddScore(int score);
    }
}