using UnityEngine;

namespace App.Code.Game
{
    public class Coin : MonoBehaviour
    {
        public int Score = 1;
        public GameObject DestroyVfx;

        private void OnDestroy() =>
            Instantiate(DestroyVfx, transform.position, transform.rotation);
    }
}