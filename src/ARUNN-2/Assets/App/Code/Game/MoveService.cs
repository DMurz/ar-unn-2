﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace App.Code.Game
{
    public class MoveService : MonoBehaviour,
        IMoveService
    {
        public float Speed => _speed;

        public event Action OnMoveUp;
        public event Action OnMoveLeft;
        public event Action OnMoveRight;
        public event Action OnMoveDown;

        [SerializeField] private float _speed;
        [SerializeField] private float _maxSpeed;

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        public void IncreaseSpeed() =>
            _speed = _maxSpeed;

        public void MoveUp() =>
            OnMoveUp?.Invoke();

        public void MoveLeft() =>
            OnMoveLeft?.Invoke();

        public void MoveRight() =>
            OnMoveRight?.Invoke();

        public void MoveDown() =>
            OnMoveDown?.Invoke();
    }
}