﻿using App.Code.Dependencies;
using UnityEngine;

namespace App.Code.Game
{
    public class ImageMarkerEntity : MonoBehaviour
    {
        private static IMoveService MoveService =>
            Locator.Get<IMoveService>();

        private void Awake() =>
            MoveService.IncreaseSpeed();
    }
}