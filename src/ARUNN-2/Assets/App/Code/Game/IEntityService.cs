﻿namespace App.Code.Game
{
    public interface IEntityService
    {
        public void EntitySpawned();
        public void EntityDestroyed();
    }
}