﻿using App.Code.Dependencies;
using UnityEngine;
using UnityEngine.UI;

namespace App.Code.Game.UI
{
    public class ScoreView : MonoBehaviour
    {
        public Text ScoreText;

        private static IScoreService ScoreService =>
            Locator.Get<IScoreService>();

        private void Awake() =>
            ScoreService.OnScoreChanged += ScoreChanged;

        private void OnDestroy() =>
            ScoreService.OnScoreChanged -= ScoreChanged;

        private void ScoreChanged(int newScore) =>
            ScoreText.text = newScore.ToString();
    }
}