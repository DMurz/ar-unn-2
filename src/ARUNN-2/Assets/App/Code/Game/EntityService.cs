using App.Code.Dependencies;
using App.Code.UI;

namespace App.Code.Game
{
    public class EntityService : IEntityService
    {
        private static IMediator Mediator =>
            Locator.Get<IMediator>();

        private int _count;

        public void EntitySpawned()
        {
            _count++;
            if (_count == 1)
                Mediator.ShowButtons();
        }

        public void EntityDestroyed()
        {
            _count--;
            if (_count == 0)
                Mediator.HideButtons();
        }
    }
}