using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace App.Code.Input
{
    public class ScreenService : MonoBehaviour,
        IPointerDownHandler, IPointerUpHandler, IScreenService
    {
        private class Touch : ITouch
        {
            public int Id { get; }
            public TouchPhase Phase { get; }
            public Vector2 Position { get; }

            public Touch(PointerEventData eventData, TouchPhase phase)
            {
                Phase = phase;
                Position = eventData.position;
                Id = eventData.pointerId;
            }
        }

        public event Action<ITouch> OnTouch;

        public void OnPointerDown(PointerEventData eventData) =>
            OnTouch?.Invoke(new Touch(eventData, TouchPhase.Began));

        public void OnPointerUp(PointerEventData eventData) =>
            OnTouch?.Invoke(new Touch(eventData, TouchPhase.Ended));
    }
}