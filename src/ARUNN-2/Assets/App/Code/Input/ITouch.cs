using UnityEngine;

namespace App.Code.Input
{
    public interface ITouch
    {
        public int Id { get; }
        public TouchPhase Phase { get; }
        public Vector2 Position { get; }
    }
}