﻿using System;

namespace App.Code.Input
{
    public interface IScreenService
    {
        public event Action<ITouch> OnTouch;
    }
}