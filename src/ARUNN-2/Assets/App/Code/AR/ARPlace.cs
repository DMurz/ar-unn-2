﻿using System;
using App.Code.Core;
using App.Code.Dependencies;
using App.Code.Input;
using App.Code.Interaction;
using UnityEngine;

namespace App.Code.AR
{
    public class ARPlace : MonoBehaviour
    {
        public GameObject HorizontalPlanePrefab;
        public GameObject VerticalPlanePrefab;

        private static IRaycastService RaycastService =>
            Locator.Get<IRaycastService>();

        private static IValidTouchService ScreenService =>
            Locator.Get<IValidTouchService>();

        private void Awake() => 
            ScreenService.OnTouch += OnTouch;

        private void OnDestroy() => 
            ScreenService.OnTouch -= OnTouch;

        private void OnTouch(ITouch touch)
        {
            if (touch.Phase == TouchPhase.Ended)
            {
                var position = touch.Position;
                var hit = RaycastService.Raycast(position);

                var isHit = hit != null;
                if (isHit)
                {
                    if (hit.Type == PlaneType.Vertical)
                        Instantiate(VerticalPlanePrefab, hit.Point, hit.Rotation);
                    else
                        Instantiate(HorizontalPlanePrefab, hit.Point, hit.Rotation);
                }
            }
        }
    }
}