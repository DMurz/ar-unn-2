﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Code.Dependencies;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace App.Code.AR
{
    public class MultipleImageTracking : MonoBehaviour
    {
        [Serializable]
        public struct NamePrefab
        {
            public string Name;
            public GameObject Prefab;
        }

        public NamePrefab[] Prefabs;

        private Dictionary<string, GameObject> _prefabs;
        private Dictionary<string, GameObject> _instances;

        private static ARTrackedImageManager TrackedImageManager =>
            Locator.Get<ARTrackedImageManager>();

        private void Awake()
        {
            TrackedImageManager.trackedImagesChanged += ImagesUpdate;
            _prefabs = Prefabs.ToDictionary(x => x.Name, x => x.Prefab);
            _instances = new Dictionary<string, GameObject>();
        }

        private void Start() { }

        private void OnDestroy() =>
            TrackedImageManager.trackedImagesChanged -= ImagesUpdate;

        private void ImagesUpdate(ARTrackedImagesChangedEventArgs args)
        {
            foreach (var trackedImage in args.added)
                ImageAdded(trackedImage.referenceImage.name, trackedImage.transform);

            foreach (var trackedImage in args.updated)
                ImageUpdated(trackedImage.referenceImage.name, trackedImage.transform, trackedImage.size,
                    trackedImage.trackingState);

            foreach (var trackedImage in args.removed)
                ImageRemoved(trackedImage.referenceImage.name);
        }

        public void ImageAdded(string imageName, Transform imageTransform)
        {
            var instance = Instantiate(_prefabs[imageName]);
            instance.transform.position = imageTransform.position;
            instance.transform.rotation = imageTransform.rotation;

            _instances.Add(imageName, instance);
        }

        public void ImageUpdated(string imageName, Transform imageTransform, Vector2 imageSize,
            TrackingState trackingState)
        {
            var instance = _instances[imageName];
            instance.transform.position = imageTransform.position;
            instance.transform.rotation = imageTransform.rotation;

            instance.transform.localScale = Vector3.one * Mathf.Max(imageSize.x, imageSize.y);

            instance.SetActive(trackingState == TrackingState.Tracking);
        }

        public void ImageRemoved(string imageName)
        {
            Destroy(_instances[imageName]);
            _instances.Remove(imageName);
        }

        [Button(ButtonStyle.FoldoutButton)]
        private void SpawnImage(XRReferenceImageLibrary lib, int index)
        {
            var go = new GameObject(lib[index].name);

            var imageName = lib[index].name;
            var instance = Instantiate(_prefabs[imageName]);
            instance.transform.position = go.transform.position;
            instance.transform.rotation = go.transform.rotation;

            _instances.Add(imageName, instance);
        }
    }
}