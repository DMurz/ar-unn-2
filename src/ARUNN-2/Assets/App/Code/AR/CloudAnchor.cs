﻿using App.Code.Dependencies;
using Google.XR.ARCoreExtensions;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace App.Code.AR
{
    public class CloudAnchor : MonoBehaviour
    {
        [Range(0, 365)] public int Days = 365;

        private static ARAnchorManager AnchorManager =>
            Locator.Get<ARAnchorManager>();
        
        private ARAnchor _anchor;
        private HostCloudAnchorPromise _promise;

        private void Awake()
        {
            _anchor = gameObject.AddComponent<ARAnchor>();
            _promise = AnchorManager.HostCloudAnchorAsync(_anchor, Days);
        }

        private void Update()
        {
            switch (_promise.State)
            {
                case PromiseState.Pending:
                    Debug.Log("Pending");
                    break;
                case PromiseState.Cancelled:
                    Debug.Log($"Cancelled: {_promise.Result.CloudAnchorState}");
                    break;
                case PromiseState.Done:
                    Debug.Log($"{_promise.Result.CloudAnchorId}");
                    break;
            }
        }
    }
}