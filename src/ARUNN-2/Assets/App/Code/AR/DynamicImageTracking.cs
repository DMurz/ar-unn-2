﻿using System;
using App.Code.Core;
using App.Code.Dependencies;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace App.Code.AR
{
    public class DynamicImageTracking : MonoBehaviour
    {
        [Serializable]
        public struct ImageData
        {
            public string Name;
            public string Url;
            public float Width;
        }

        public ImageData[] Images;

        private static IImageService ImageService =>
            Locator.Get<IImageService>();

        private static ARTrackedImageManager TrackedImageManager =>
            Locator.Get<ARTrackedImageManager>();

        private MutableRuntimeReferenceImageLibrary _imageLibrary;
        private ImageJobHandler _imageJobHandler;

#if !UNITY_EDITOR
        private void Start()
        {
            _imageLibrary = TrackedImageManager.CreateRuntimeLibrary() as MutableRuntimeReferenceImageLibrary;
            TrackedImageManager.referenceLibrary = _imageLibrary;
            _imageJobHandler = new ImageJobHandler(TrackedImageManager, _imageLibrary);

            foreach (var imageData in Images)
            {
                var id = imageData;
                ImageService.GetImage(imageData.Url, tex => ImageLoaded(tex, id));
            }
        }

        private void Update() => 
            _imageJobHandler.Run();
#else
        private void Start() { }
#endif
        private void ImageLoaded(Texture2D image, ImageData data) =>
            _imageJobHandler.Add(data.Name, data.Width, image);
    }
}