﻿using System.Collections.Generic;
using App.Code.Core;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace App.Code.AR
{
    public class ARRaycastService : IRaycastService
    {
        private class RaycastHit : IRaycastHit
        {
            public PlaneType Type { get; }
            public Vector3 Point { get; }
            public Quaternion Rotation { get; }
            public GameObject Target { get; }

            private ARRaycastHit _arRaycastHit;

            public RaycastHit(ARRaycastHit arRaycastHit)
            {
                _arRaycastHit = arRaycastHit;

                if (arRaycastHit.trackable is ARPlane plane)
                {
                    switch (plane.alignment)
                    {
                        case PlaneAlignment.Vertical:
                            Type = PlaneType.Vertical;
                            break;
                        case PlaneAlignment.HorizontalUp:
                        case PlaneAlignment.HorizontalDown:
                            Type = PlaneType.Horizontal;
                            break;
                        default:
                            Type = PlaneType.Null;
                            break;
                    }
                }

                Point = _arRaycastHit.pose.position;
                Rotation = _arRaycastHit.pose.rotation;
                Target = arRaycastHit.trackable.gameObject;
            }
        }
        
        private readonly List<ARRaycastHit> _buffer;
        private readonly ARRaycastManager _raycastManager;

        public ARRaycastService(ARRaycastManager raycastManager)
        {
            _raycastManager = raycastManager;
            _buffer = new List<ARRaycastHit>(5);
        }

        public IRaycastHit Raycast(Vector2 screenPos) =>
            _raycastManager.Raycast(screenPos, _buffer, TrackableType.PlaneWithinBounds) 
                ? new RaycastHit(_buffer[0]) 
                : null;
    }
}