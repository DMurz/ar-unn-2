﻿using App.Code.Dependencies;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.XR.ARSubsystems;

namespace App.Code.AR
{
    public class EditorImageMarker : MonoBehaviour
    {
        public string Name;
        public Vector2 Size;
        public TrackingState State;

        private bool _isStarted;

        private static MultipleImageTracking MultipleImageTracking =>
            Locator.Get<MultipleImageTracking>();

        private void Update()
        {
            if (_isStarted)
                MultipleImageTracking.ImageUpdated(Name, transform, Size, State);
        }

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        private void StartTracking()
        {
            State = TrackingState.Tracking;
            MultipleImageTracking.ImageAdded(Name, transform);
            _isStarted = true;
        }

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        private void ResumeTracking() => 
            State = TrackingState.Tracking;

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        private void PauseTracking() => 
            State = TrackingState.Limited;

        [Button(ButtonStyle.FoldoutButton), DisableInEditorMode]
        private void StopTracking()
        {
            State = TrackingState.None;
            MultipleImageTracking.ImageRemoved(Name);
            _isStarted = false;
            Destroy(gameObject);
        }
    }
}
