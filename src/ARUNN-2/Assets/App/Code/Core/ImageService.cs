using System;
using System.Net;
using System.Net.Http;
using UnityEngine;

namespace App.Code.Core
{
    public class ImageService : IImageService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public ImageService()
        {
            ServicePointManager.ServerCertificateValidationCallback = 
                (sender, certificate, chain, errors) => true;
        }
        
        public async void GetImage(string url, Action<Texture2D> callback)
        {
            var bytes = await _httpClient.GetByteArrayAsync(url);
            var texture = new Texture2D(2, 2);
            texture.LoadImage(bytes);
            texture.Apply();
            callback?.Invoke(texture);
        }
    }
}