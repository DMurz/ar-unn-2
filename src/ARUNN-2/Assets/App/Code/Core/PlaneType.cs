namespace App.Code.Core
{
    public enum PlaneType
    {
        Null,
        Horizontal,
        Vertical
    }
}