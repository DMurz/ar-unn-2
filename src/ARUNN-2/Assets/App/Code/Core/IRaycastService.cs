﻿using UnityEngine;

namespace App.Code.Core
{
    public interface IRaycastService
    {
        public IRaycastHit Raycast(Vector2 screenPos);
    }
}