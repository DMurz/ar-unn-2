﻿using System;
using UnityEngine;

namespace App.Code.Core
{
    public interface IImageService
    {
        public void GetImage(string url, Action<Texture2D> callback);
    }
}