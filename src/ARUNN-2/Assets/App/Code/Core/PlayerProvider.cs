﻿using UnityEngine;

namespace App.Code.Core
{
    public class PlayerProvider : MonoBehaviour,
        IPlayerProvider
    {
        public Camera Player => PlayerCamera;

        public Camera PlayerCamera;
    }
}