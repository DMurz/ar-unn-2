using UnityEngine;

namespace App.Code.Core
{
    public interface IRaycastHit
    {
        public PlaneType Type { get; }
        public Vector3 Point { get; }
        public Quaternion Rotation { get; }
        public GameObject Target { get; }
    }
}