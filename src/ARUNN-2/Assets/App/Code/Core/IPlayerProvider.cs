using UnityEngine;

namespace App.Code.Core
{
    public interface IPlayerProvider
    {
        public Camera Player { get; }
    }
}